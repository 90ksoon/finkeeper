<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTTransactionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('accounts', function (Blueprint $table) {
            $table->foreign('strategy_id')->references('id')->on('strategy')->onDelete('cascade');

        });

        Schema::table('strategy', function (Blueprint $table) {
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });

        Schema::create('t_transaction_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('t_type_name');
            $table->text('t_type_desc');
            $table->string('t_income_flag');
            $table->string('t_expenses_flag');
            $table->string('t_budget_flag');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('strategy', function (Blueprint $table) {
            $table->dropForeign('strategy_account_id_foreign');
        });
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign('accounts_strategy_id_foreign');
        });

        Schema::dropIfExists('t_transaction_types');
    }
}
