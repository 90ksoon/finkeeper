<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransubtypeInTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table){

          $table->integer('transaction_subtypes_id')->unsigned();
          $table->string('remark_keyword');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('transactions', function (Blueprint $table){

        $table->dropColumn('transaction_subtypes_id');
        $table->dropColumn('remark_keyword');

      });
    }
}
