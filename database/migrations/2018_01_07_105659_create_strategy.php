<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrategy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Strategy table to record strategy used by accoutn & user */
        Schema::create('accounts', function (Blueprint $table){
          $table->increments('id');
          $table->integer('strategy_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('strategy', function (Blueprint $table){
          $table->increments('id');
          $table->text('desc');
          $table->integer('account_id')->unsigned();
          $table->integer('type');
          $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('accounts', function (Blueprint $table) {
          $table->dropForeign('accounts_user_id_foreign');
        });
        Schema::dropIfExists('accounts');
        Schema::dropIfExists('strategy');
    }
}
