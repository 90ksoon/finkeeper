<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::create('transactions', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->integer('user_id')->unsigned();
          $table->integer('transaction_type_id')->unsigned();
          $table->double('amount');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
          $table->softDeletes();
      });

        Schema::create('transaction_types', function (Blueprint $table) {
            $table->increments('id');
            $table->text('transtype_name');
            $table->text('transtype_desc')->nullable();
            $table->string('income');
            $table->string('expenses');
            $table->string('budget')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('transaction_types', function (Blueprint $table){
          $table->dropForeign('transaction_types_transaction_id_foreign');
          $table->dropForeign('transaction_types_user_id_foreign');
        });
        Schema::dropIfExists('transaction_types');

        Schema::table('transactions', function(Blueprint $table){
          $table->dropForeign('transactions_user_id_foreign');
        });

        Schema::dropIfExists('transactions');
    }
}
