<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrategyType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    /** Strategy table for recording type of strategy deployed */
     Schema::create('strategy_type',function (Blueprint $table){
       $table->increments('id')->unique();
       $table->text('desc');
       $table->text('methods');
       $table->timestamps();
     });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strategy_type');
    }
}
