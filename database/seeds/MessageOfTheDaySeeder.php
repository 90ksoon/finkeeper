<?php

use Illuminate\Database\Seeder;
use App\MOTD;
use Illuminate\Support\Facades\DB;

class MessageOfTheDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('m_o_t_d_s')->insert([
        'message'=>'It Is Not the Strongest of the Species that Survives But the Most Adaptable - Charles Darwins',
        'created_at'=> date('Y-m-d H:i:s')
        ]);

        DB::table('m_o_t_d_s')->insert([
        'message'=>'Science is interesting, and if you don\'t agree you can f*** off - Richard Dawkins',
        'created_at'=> date('Y-m-d H:i:s')
        ]);

        DB::table('m_o_t_d_s')->insert([
        'message'=>'To climb steep hills requires slow pace at first - William Shakespeare',
        'created_at'=> date('Y-m-d H:i:s')
        ]);
        DB::table('m_o_t_d_s')->insert([
        'message'=>'Dont gain the world and lose your soul, wisdom is better than silver or gold... - Bob Marley',
        'created_at'=> date('Y-m-d H:i:s')
        ]);
        DB::table('m_o_t_d_s')->insert([
        'message'=>"No man is good enough to govern another man without the other's consent - Abraham Lincoln ",
        'created_at'=> date('Y-m-d H:i:s')
        ]);
        DB::table('m_o_t_d_s')->insert([
        'message'=>"No man is good enough to govern another man without the other's consent - Abraham Lincoln ",
        'created_at'=> date('Y-m-d H:i:s')
        ]);
    }
}
