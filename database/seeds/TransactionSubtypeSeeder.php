<?php

use Illuminate\Database\Seeder;
use App\TransactionSubtype;

class TransactionSubtypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    factory( TransactionSubtype::class, 30 )->create();

    }
}
