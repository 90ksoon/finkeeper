<?php

use Illuminate\Database\Seeder;


class TTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert entries into default transaction template
        $itemName = ['Savings','Salary','Bonus','Investment','Food','Petrol','Clothings','Utilities','Entertainment','Health Care', 'Personal Care', 'Rental/Mortgage', 'Loan', 'Investment'];
        $itemDesc = ['Personal savings or Family Savings','','Additional Income','Returns of Investment or Business','Yummy!','Didn\'t need this if its an Electric','Fashion speaks LOUD','Pain in the a*s','Woo Hoo!','This is Important', 'Care personally', 'Bigger Pain in the a**', 'Deal with it', 'Be Patience'];
        $itemIncomeFlag = ['Y','Y','Y','Y','N','N','N','N','N','N','N','N','N','N'];
        $itemExpenseFlag = ['N','N','N','N','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y'];
        $itemBudgetFlag = ['N','N','N','N','N','N','N','N','N','N','N','N','N','N'];

        $countArray = [];
        for($count = 1; $count <= count($itemName); $count++){
          $countArray[$count-1] = $count;
        }

        //array map calls function within class inside must be that way
        array_map(array('TTransactionSeeder','mapTTransactionTable'), $countArray, $itemName,$itemDesc,$itemIncomeFlag,$itemExpenseFlag,$itemBudgetFlag);

    }

    //map into template transaction table
    function mapTTransactionTable($count,$a,$b,$c,$d,$e)
    {

      DB::table('t_transaction_types')->insert([
        'id'=> $count,
        't_type_name'=> $a,
        't_type_desc'=> $b,
        't_income_flag'=> $c,
        't_expenses_flag' => $d,
        't_budget_flag' => $e,
        'created_at' => date("Y-m-d H:i:s")
      ]);

    }
}
