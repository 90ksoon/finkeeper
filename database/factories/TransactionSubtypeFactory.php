<?php

use Faker\Generator as Faker;

$factory->define(App\TransactionSubtype::class, function (Faker $faker) {

    $subname = ['little expenses', 'habitual', 'funny', 'exciting'];
    return [
        'transaction_type_id' => $faker->numberBetween(1, 8),
        'transubtype_name' => $faker->randomElement($subname),
        'user_id' => $faker->numberBetween(1, 3),
    ];
});
