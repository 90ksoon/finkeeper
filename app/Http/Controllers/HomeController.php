<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\TransactionType;
use App\TTransactionType;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaction_type = $this->getTransactionType();

        $user = auth()->user();

        // obtain transactions for the current user
        $transaction = (new Transaction)->dailyTransaction($user);
        // get the transaction existence
        if ($transaction_type->count() == 0) {

            $newSetup = TTransactionType::all();

            return view('home')->with(compact(['transaction_type', 'newSetup']));
        }
        else {

          return view('home')->with(compact('transaction_type', 'transaction'));

        }

    }

    private function getTransactionType()
    {
        $transaction_type = new TransactionType;

        $value = $transaction_type->where('user_id', auth()->user()->id)->get();
        if ($value) {
            return $value;
        }

    }

    public function getCharting($chart)
    {
      $transaction_type = $this->getTransactionType();

      $user = auth()->user();
      $impulsive;

      if ($chart == 'savings') {
        // obtain transactions for the current user
        $transaction = (new Transaction)->dailySavings($user);
      }elseif ($chart == 'expenses') {
        $transaction = (new Transaction)->dailyExpenses($user);
      }
      elseif ($chart == 'impulsive'){
        $transaction = (new Transaction)->dailyImpulsive($user);
        $impulsive = 'Y';
      }else{
        redirect('home');
      }

      // get the transaction existence
      if ($transaction_type->count() == 0) {

          $newSetup = TTransactionType::all();

          return view('home')->with(compact(['transaction_type', 'newSetup']));
      }
      else {

        return view('home')->with(compact('transaction_type', 'transaction', 'impulsive'));

      }
    }


}
