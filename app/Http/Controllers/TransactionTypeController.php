<?php

namespace App\Http\Controllers;

use App\TransactionType;
use App\TTransactionType;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;

class TransactionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'transactionType' => 'required',

        ]);

        if ($validator->fails()) {

            if ($request->ajax()) {

                $json = [];

                return Response::json(
                    ['success' => false,
                        'errors' => $validator->getMessageBag()->toArray()]
                );
            } else {
                return back()->withErrors($validator);
            }

        } else {

          if(is_array($request->get('transactionType'))){

            foreach ($request->get('transactionType') as $input) {

              $transaction = new TransactionType;
              $tranTemplate = (new TTransactionType)->where('t_type_name', '=', $input)->get();
              $transaction->transtype_name = $input;
              $transaction->transtype_desc = $input;
              $transaction->income = $tranTemplate->pluck('t_income_flag')[0];
              $transaction->expenses = $tranTemplate->pluck('t_expenses_flag')[0];
              $transaction->budget = $tranTemplate->pluck('t_budget_flag')[0];
              $transaction->user_id = Auth::id();
              if (Gate::allows('post-transactiontype', $transaction)) {

                $transaction->save();

              } else {

                abort(403, 'You Do Not Have Permission to Create on Behalf');

              }

            }
          }
          else{
            $transaction = new TransactionType;
            $input = $request->get('transactionType');
            $option = $request->get('transactionDesc');
            if($option == 'Y'){
              $income = 'Y';
              $expenses = 'N';
            }else{
              $income = 'N';
              $expenses = 'Y';
            }
            $transaction->transtype_name = $input;
            $transaction->transtype_desc = $input;
            $transaction->income = $income;
            $transaction->expenses = $expenses;
            $transaction->budget = 'KIV';
            $transaction->user_id = Auth::id();
            if (Gate::allows('post-transactiontype', $transaction)) {

              $transaction->save();

            } else {

              abort(403, 'You Do Not Have Permission to Create on Behalf');

            }
          }

          if ($request->ajax()) {

              $json = [];

              return Response::json(
                  ['success' => true]
              );
          } else {

            return redirect('home');

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
