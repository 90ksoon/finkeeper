<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User as User;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Response;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'amount' => 'required|numeric',
                'transaction_type' => [
                    'required',
                    Rule::exists('transaction_types', 'id')->where(function ($query) {
                        $query->where('user_id', auth()->user()->id);
                    }),
                ],
            ],
            $message = ['transaction_type', 'transaction type must exists, please add']
        );

        //fails to validate
        if ($validator->fails()) {

            if ($request->ajax()) {

                $json = [];

                return Response::json(
                    ['success' => false,
                        'errors' => $validator->getMessageBag()->toArray()]
                );

            } else {
                return back()->withErrors($validator);
            }

        } else {

            $transaction = new Transaction;

            $transaction->amount = $request->input('amount');

            $transaction->transaction_type_id = $request->input('transaction_type');

            $transaction->transaction_subtypes_id = $request->input('transaction_subtypes');

            $transaction->user_id = auth()->id();

            $remarks = $request->input('remarks'); //potentially trailing NULL value coming from taggingJS

            if(isset($remarks)){

              $remarks = implode("," , array_filter($remarks));

            }

            $transaction->remark_keyword = $remarks;



            if (Gate::allows('post-transaction', $transaction)) {

                $transaction->save();

                if (!$transaction) {
                  App::abort(500, 'Error');
                }

                if ($request->ajax()) {

                    $json = ['status' => 'success'];
                    return Response::json($json);
                    // return header('X-SAVED-DATA-RESPONSE', true, 302);
                } else {

                    return back();

                }

            } else {

                abort(403, 'You Do Not Have Permission to Add Transaction on behalf');

            }

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
