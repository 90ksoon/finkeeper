<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //
    public function show($id){

      $user = User::find($id);

      return view('user')->with('user',$user);

    }


    public function public()
    {
      dd(auth()->user());
    }
}
