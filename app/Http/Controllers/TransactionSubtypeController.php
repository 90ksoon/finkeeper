<?php

namespace App\Http\Controllers;

use App\TransactionSubtype;
use Illuminate\Http\Request;

class TransactionSubtypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      $userid = auth()->user()->id;

      $subType = (new TransactionSubtype)->where('user_id','=',$userid)->where('transaction_type_id','=',$id);

      $subListing = $subType->select('id','transubtype_name')->get();

        return $subListing;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransactionSubtype  $transactionSubtype
     * @return \Illuminate\Http\Response
     */
    public function show(TransactionSubtype $transactionSubtype)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransactionSubtype  $transactionSubtype
     * @return \Illuminate\Http\Response
     */
    public function edit(TransactionSubtype $transactionSubtype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransactionSubtype  $transactionSubtype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransactionSubtype $transactionSubtype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransactionSubtype  $transactionSubtype
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionSubtype $transactionSubtype)
    {
        //
    }
}
