<?php

namespace App\Http\Controllers;

use App\TransactionType;
use App\User as User;

class DashBoardController extends Controller
{
    public function show()
    {

        // $transactionType = User::findOrFail($id)->transactionType();

        $user = auth()->user();

        return view('dashboard', compact('user'));

    }

    public function create()
    {
        $this->case = 'Hello World';
    }
}
