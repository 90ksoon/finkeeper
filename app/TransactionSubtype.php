<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionSubtype extends Model
{
    public function transactionType()
    {
      return $this->belongsToMany(TransactionType::class);
    }
}
