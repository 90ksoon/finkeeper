<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    public function user(){
      return $this->hasOne(User::class);
    }

    public function strategy(){
      return $this->hasOne(Strategy::class);
    }
}
