<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{

    protected $guarded = [];
    protected $table = 'transactions';
    //
    public function transactiontype(){
      return $this->hasOne(TransactionType::class);
    }

    public function user(){
      return $this->belongsTo(User::class);
    }

    /**
    * Check transaction exists
 	 *
 	 * @param
 	 * @return bool true or false
	 */
    public function hasTransactions(User $user)
    {
      return Transaction::where('user','=',$user->id);
    }


    /**
 	 * Obtain daily transactions
 	 *
 	 * @param void
 	 * @return string
	 */

    public function dailyTransaction(User $user)
    {

      $transactions = DB::select('select date(a.created_at) as date, transtype_name, sum(a.amount) as amount, income from transactions a join transaction_types b on b.id = a.transaction_type_id where a.user_id = ? group by date(a.created_at), transtype_name, income',[$user->id]);


      return $transactions;

    }

    public function dailySavings(User $user)
    {
      $transactions = DB::select("select date(a.created_at) as date, transtype_name, sum(a.amount) as amount, income from transactions a join transaction_types b on b.id = a.transaction_type_id where a.user_id = ? and income = 'Y' group by date(a.created_at), transtype_name, income",[$user->id]);


      return $transactions;

    }

    public function dailyExpenses(User $user)
    {
      $transactions = DB::select("select date(a.created_at) as date, transtype_name, sum(a.amount) as amount, income from transactions a join transaction_types b on b.id = a.transaction_type_id where a.user_id = ? and income = 'N' and expenses = 'Y' group by date(a.created_at), transtype_name, income",[$user->id]);


      return $transactions;

    }

    public function dailyImpulsive(User $user)
    {
      $transactions = DB::select("select date(a.created_at) as date, remark_keyword as transtype_name, sum(a.amount) as amount, income from transactions a join transaction_types b on b.id = a.transaction_type_id where a.user_id = ? and expenses = 'Y' group by date(a.created_at), remark_keyword, income",[$user->id]);

      $i = 0;
      foreach ($transactions as $trans) {
        $i++;
        if(strpos($trans->transtype_name,',') ){
          $dupArray = [];
          $name = explode(',' , $trans->transtype_name);

          foreach($name as $key => $value){
            $transactions[count($transactions)] = [
              'date'=>$trans->date,
              'transtype_name'=>$value,
              'amount'=>$trans->amount,
              'income'=>$trans->income
            ];
          }
          array_splice($transactions, $i-1, 1);
        }
      }
      return $transactions;

    }

}
