<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MOTD extends Model
{
    protected $fillable = ['message'];

    public function getMessageAttribute(){

      $message = DB::table('m_o_t_d_s')->pluck('message')->inRandomOrder()->first();

      return $message;

    }

}
