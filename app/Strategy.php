<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Strategy extends Model
{
    //
    public function account(){
      return $this->belongsTo(Account::class);
    }

    public function strategytype(){
      return $this->hasMany(StrategyType::class);
    }
}
