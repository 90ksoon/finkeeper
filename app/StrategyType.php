<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StrategyType extends Model
{
    //
    public function strategy(){
      return $this->belongsTo(Strategy::class);
    }
}
