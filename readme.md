## Finance Keeper
### by [Byron Wong](https://www.linkedin.com/in/byron-wong-580772116/) 23 Oct 2017
---
## Pre-requisites
- Apache Web Server (lampp or [xampp](https://www.apachefriends.org/index.html))
- PHP version 7.0+
- Composer for PHP

## Installation
1. Download to working path in Apache htdocs
2. Update & Install composer `composer update` followed by `composer install`
3. Ensure `/storage` and `bootstrap/cache` has executable permission by webserver
4. run `cp .env.example .env` then `php artisan key:generate` to generate key
5. Run `php artisan serve`
6. All set!

## License
The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
