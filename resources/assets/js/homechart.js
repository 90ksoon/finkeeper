var Chart = require('chart.js');
var chartlabel = [];
var chartdate = [];

function chartDataTransform(trans, lineprop = false){
  //handles x-axis
  for (var i = 0, row; row = trans[i]; i++) {

      if (chartdate.indexOf(row.date) == -1) {
        chartdate.push(row.date);
      }

  //handles transaction name and amount
    // chart name insert
    if (chartlabel.indexOf(row.transtype_name) == -1){
      //new transaction type
      chartlabel.push(row.transtype_name);

    }

  }
  var chartDataSet = {
    labels: chartdate,
    datasets: []
  }
  // loop for each labels

  var chartD = new Array;
  var chartind = new Array;
  chartlabel.forEach(function(chartlabel1, k){


      chartD[k] = new Array;
      chartind[k] = new Array;
      // loop for each date
      chartdate.forEach(function(chartdate1, j){

          // date of the label doesnt match the
          for (var m = 0; m < trans.length; m++) {
            //dealing for each dataset labels
            // if data different date but same label name
            if(chartdate1 == trans[m].date){

              //colorise and adjust amount positivity
              var red = 'rgb(203, 47, 98)';
              var green = 'rgb(41, 222, '+ (Math.floor(Math.random() * 150) + 100 )+')';
              var color = green;


              if(chartdate1 == trans[m].date && trans[m].transtype_name == chartlabel1 ){
                // not income grouping
                if (trans[m].income == 'N' && trans[m].transtype_name == chartlabel1 || lineprop != false) {
                  trans[m].amount = trans[m].amount * -1;
                  color = red;
                }
                //push into amount
                chartD[k][j] = trans[m].amount;
                chartind[k][j] = color;
              }else if(chartdate1 != trans[m].date && trans[m].transtype_name == chartlabel1) {

                chartD[k][j] = '0';
                chartind[k][j] = color;
              }
              else {
                chartind[k][j] = color;

              }
            }

          }
      });

      if(lineprop){
        chartDataSet.datasets.push({
          label: chartlabel[k],
          backgroundColor: chartind[k],
          data: chartD[k],
          fill: false,
          pointRadius: 10,
          pointHoverRadius: 15,
          showLine: false
        });

      }else{
        chartDataSet.datasets.push({
          label: chartlabel[k],
          backgroundColor: chartind[k],
          data: chartD[k],
        });

      }
  });

  return chartDataSet;

}

/* Adapter to call charts*/
function outputChart(charttype, charts){
  var ctx = document.getElementById("homeCanvas").getContext('2d');
  var chartmake;
  switch (charttype) {
    case 'bar':
      chartmake = standardBarChart('bar',charts,false);
      break;
    case 'stackedbar':
      chartmake = standardBarChart('bar',charts,true);
      break;
    case 'line':
      chartmake = standardBarChart('line',charts,false);
      break;
    case 'dots':
      chartmake = linePoint('circle',charts);
      break;
    default:
      chartmake = standardBarChart('bar',charts,true);
  }

  var myChart = new Chart(ctx, chartmake);
  return myChart;
}

/*Output standard bar charting*/
function standardBarChart(ztype, charts, stackbool = false) {
  return {
    type: ztype,
    data: charts,
    options: {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          stacked: stackbool,
        }],
        xAxes: [{
          stacked: stackbool,
          gridLines: false
        }]
      },
      responsive: true
    }
  };
}

/*Output line chart in dots*/
function linePoint(pointStyle, inputcharts) {

  var charts = chartDataTransform(inputcharts, true);
			return {
				type: 'line',
				data: charts,
				options: {
					responsive: true,
					title: {
						display: true,
						text: 'Adhoc / Special'
					},
					legend: {
						display: false
					},
					elements: {
						point: {
							pointStyle: pointStyle
						}
					}
				}
			};
		}

/* To Reset Canvas when hover will not have problem*/
function resetCanvas() {
  $('#homeCanvas').remove();
  $('#chart-container').append("<canvas id='homeCanvas'></canvas>");
}

$( document ).ready(function() {

    $('#stackedbar').click(function(){
      resetCanvas();
      outputChart('stackedbar', chartDataSet,true);

    });
    $('#verticlebar').click(function(){
      resetCanvas();
      outputChart('bar', chartDataSet,false);
    });
    $('#linechart').click(function(){
      resetCanvas();
      outputChart('line', chartDataSet,false);
    });
    var o = 0;
    $('#budget').click(function(){
      resetCanvas();
      var option = ['bar','stackedbar','line'];
      outputChart(option[o], chartDataSet,false);
      o++;
      if(o > option.length ) o = 0;

    });

    var chartDataSet = chartDataTransform(trans);
    if(defchart == 'dot'){
      outputChart('dots', trans);
      // $(".chart-container").css("height","50");
    }else {
      outputChart('stackedbar', chartDataSet, true);
    }
});
