$(document).ready(function() {

  //TransactionType
  $(document).on('submit', '#transactionTypeForm', function(event) {

    event.preventDefault();
    var errorDialog = $('#errors');

    $.ajax({
        url: "/transaction/newType",
        type: "post",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('#transactionTypeForm').serialize(),
      })
      .done(function(xml, text, response) {
        // output error msg
        if (response.responseJSON.success == false) {
          errorDialog.attr('class', 'alert alert-danger');
          $.each(response.responseJSON.errors, function(key, error) {
            errorDialog.find('ul').append('<li>' + error + '</li>');
          });
        } else {
          //redirect
          window.location.href = "/home"
        }
      });
  });

  //New Transaction
  $(document).on('submit', '#transactionForm', function(event) {

    event.preventDefault();
    var errorDialog = $('#errors');

    $.ajax({
        url: "/transaction/new",
        type: "post",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('#transactionForm').serialize(),
      })
      .done(function(xml, text, response) {
        // output error msg
        if (response.responseJSON.success == false) {
          errorDialog.attr('class', 'alert alert-danger');
          $.each(response.responseJSON.errors, function(key, error) {
            errorDialog.find('ul').append('<li>' + error + '</li>');
          });
        } else {
          //redirect
          window.location.href = "/home"
        }
      });
  });

  $('#star').click(function() {
    $('#addTransaction,#tranType,#budget').toggleClass('collapse visible-xs');
  });

  $('#general_settings').click(function() {
    $('#row2,#transaction_type, #setting1, #setting2, #regional').toggleClass('collapse');
  });

  //obtain lookup
  $('#trantype').on('change', function() {

    var picked = this.value;
    $('#transubtype').children().remove();

    $.ajax({
      url: "/transaction/checkSubtype/" + picked,
      type: "post",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: picked
    }).done(function(xml, text, response) {
      $.each(response.responseJSON, function(key, value) {
        console.log(value);
        $('#transubtype').append('<option value="' + value.id + '">' + value.transubtype_name + '</option>')
      })
    });
  });

  /**
  * Attach values of remark into input field
  */
  // $('#transactionForm').submit(function(){
  //   var tagvalues;
  //
  //   tagvalues = $('#remarks_tag').val();
  //
  //   tagvalues.forEach(function(value){
  //     console.log(value);
  //     $('#remarks_tag').append('<input value="'+ value +'" name="remarks[]" />');
  //
  //   });
  // });

  });
