@extends('layouts.master')
@section('content')

<div class="col-m-2">
</div>
<div class="col-xs-12-m-8" tabindex="-1">
  <div class="modal fade" id="dashboardModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="inputModalTitle">New Transaction</h4>
        </div>
        <div class="modal-body">
          <form class="form" role="form" action="/dashboard" method="post">
            <div class="form-group">
              <label for="transactionType">Transactions</label>
              <input type="text" class="form-control" id="transactionType" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="transactionType">Transaction Type</label>

              </div>
            <div class="form-group">
              <label for="amount">Amount</label>
              <input type="number" class="form-control" id="amount" placeholder="RM" >
            </div>

          </form>

      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </div>
  </div>
  </div>


  <div class="modal fade" id="transactionTypeModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="inputModalTitle">New Transaction Type</h4>
        </div>
        <div class="modal-body">
          <form class="form" id="transactionTypeForm" role="form" action="/dashboard/{{ $user->id or ''}}/new" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="transactionType">Transactions Type</label>
              <input type="text" class="form-control" id="transactionType" name="transactionType" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="transactionDesc">Description</label>
              <input type="text" class="form-control" id="transactionDesc" name="transactionDesc" placeholder="Descriptions" required>
            </div>
            <div class="hidden" id="errors" >
              <ul></ul>
            </div>
      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">New</button>
        </div>
      </form>
    </div>
  </div>
  </div>

  <button class="btn btn-primary" data-toggle="modal" data-target="#dashboardModal">
  New Transaction
  </button>
  <button class="btn btn-primary" data-toggle="modal" data-target="#transactionTypeModal">
  New Transaction Type
  </button>


</div> <!-- col-xs-12-m-8 -->

<div class="col-m-2">
</div>

@endsection

@section('ajax')
        <script>

    $(document).ready( function(){

      $(document).on('submit', '#transactionTypeForm', function(event){

        event.preventDefault();
        var errorDialog = $('#errors');

        $.ajax({
          url: "{{ $user->id }}/new",
          type: "post",
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          data: $('#transactionTypeForm').serialize(),
        })
        .done( function(xml, text, response){
          // output error msg
          if(response.responseJSON.success == false){
            errorDialog.attr('class','alert alert-danger');
            $.each(response.responseJSON.errors, function(key, error){
              errorDialog.find('ul').append('<li>'+error+'</li>');
            });
          }else{
            //redirect
             window.location.href = "{{ $user->id }}"
          }

        });
    });


        });

        </script>
@endsection
