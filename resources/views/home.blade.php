@extends('layouts.master') @section('content')
<div class="container-fluid">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
    </div>
  </div>

  <div class="row">
    <div class="chart-container" id="chart-container">
      <canvas id="homeCanvas"></canvas>
    </div>
  </div>

  @include('modals.newTransactionType')
  @include('modals.newTransaction')
  @include('modals.personCategory')


  <div class="row hidden-xs">
    <!--large screen-->
    <div class="row">
      <!-- first row-->
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block" id="general_settings">
          <span class='glyphicon glyphicon-cog' aria-hidden='true'></span> General Settings
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block">
          <span class='glyphicon glyphicon-calendar' aria-hidden='true'></span> Budget Planning
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn-group btn-block dropup">
          <div class="btn btn-square btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class='glyphicon glyphicon-book' aria-hidden='true'></span> Expenses Analysis
          </div>
          <ul class="dropdown-menu btn-block">
            <li><a href="savings">Savings Analysis</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="expenses">General Expenses</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="impulsive">Impulsive Expenses</a></li>
        </div>

      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block teal" data-toggle="modal" @isset($newSetup) data-target="#personCategoryModal" @else data-target="#transactionModal" @endif>
          <span class='glyphicon glyphicon-plus' aria-hidden='true'></span> Add Transaction
        </div>
      </div>

    </div>
    <!-- first row -->

    <div class="row" id="row2 collapse">
      <!-- second row -->
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block collapse" data-toggle="modal" id="regional" data-target="#">
          <span class='glyphicon glyphicon-globe' aria-hidden='true'></span> Regional Setting
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn-group btn-block dropup">
        <div class="btn btn-square btn-block collapse dropdown-toggle" data-toggle="dropdown" id="setting1" data-toggle="dropdown">
          <span class='glyphicon glyphicon-wrench' aria-hidden='true' aria-expanded="false"></span> Chart Setting
        </div>
        <ul class="dropdown-menu btn-block">
          <li><a id='stackedbar' href="#">Stacked Bar Chart</a></li>
          <li role="separator" class="divider"></li>
          <li><a id='verticlebar' href="#">Verticle Bar Chart</a></li>
          <li role="separator" class="divider"></li>
          <li><a id='linechart' href="#">Line Chart</a></li>
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block collapse" data-toggle="modal" id="setting2" data-target="">
          <span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Setting 2
        </div>
      </div>
      <div class="col-sm-3 col-xs-12">
        <div class="btn btn-square btn-block collapse" data-toggle="modal" id="transaction_type" data-target="#personCategoryModal">
          <span class='glyphicon glyphicon-list-alt' aria-hidden='true'></span> Personalize Categories
        </div>
      </div>
    </div>
    <!-- second row -->

  </div>
  <!--large screen-->


  <!--small screen-->

  <div class="mobile">
    <button class="btn-circle visible-xs" id="star">
        <span class ='glyphicon glyphicon-th-large' aria-hidden='true'></span>
      </button>
    <button class="btn-circle teal collapse" data-toggle="modal" @isset($newSetup) data-target="#personCategoryModal" @else data-target="#transactionModal" @endif id="addTransaction">
        <span class ='glyphicon glyphicon-plus' aria-hidden='true'></span>
      </button>
    <button class="btn-circle teal collapse" id="budget">
        <span class ='glyphicon glyphicon-tasks' aria-hidden='true'></span>
      </button>
    <button class="btn-circle teal collapse" data-toggle="modal" data-target="#transactionTypeModal" id="tranType">
        <span class ='glyphicon glyphicon-list-alt' aria-hidden='true'></span>
      </button>
  </div>
  <!--small screen-->



</div>
</div>
@endsection
@section('jsvar')
  @isset($transaction)
  <script>var defchart;</script>

  <script>
  @php
  $transaction = json_encode($transaction);
  echo "var trans; trans = JSON.parse('$transaction');";
  @endphp
  </script>
  @isset($impulsive)
  <script>var defchart ='dot';</script>
  @endisset
  @endisset
@endsection

@section('footer')
@endsection
