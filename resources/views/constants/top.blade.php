<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

      <!-- Mobile Menu -->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <!-- Header Icon -->
        <a  href="{{ url('/') }}">
          <img class="navbar-brand" src="{{ asset('images/Soon-head.png') }}" alt="Byron's Little Project">
        </a>

        <!-- Message Of The Day -->
        @inject('DB','Illuminate\Support\Facades\DB')
        @foreach(DB::table('m_o_t_d_s')->select('message')->inRandomOrder()->first() as $k => $v)
          <div class="navbar-text hidden-xs">
            <i>{{ $v }}</i>
          </div>
        @endforeach
        <div class="navbar-text visible-xs"><p>Byron's Little Project</p></div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

    <!--Right side of the NavBar -->
      <ul class="nav navbar-nav navbar-right">
          @guest
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
          @else
          @can('admin')
          <li><a href="{{ route('admin') }}">Admin</a></li>
          @endcan
            <li><a href="{{ url('/') }}">Main</a></li>
            <li><a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form></li>
          @endguest
      </ul>

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->

</nav>
