<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <!--meta header-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles {{ Auth::user()}}-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Title -->
    <title>Byron's Little Project</title>
</head>
<body>
    @include('constants.top')

    @yield('header')

    <div class="container-fluid">
      <!-- Contents -->
    @yield('content')

    </div>


    <!-- Scripts -->
    @yield('jsvar')
    <script src="{{ asset('js/app.js') }}"></script>
</body>
<footer>
  <div class="footer">
    @yield('footer')


    @inject('carbon','Carbon\Carbon')
    Copyright <span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span> {{ $carbon->now()->format('Y') }} Byron Wong

  </div>
</footer>
</html>
