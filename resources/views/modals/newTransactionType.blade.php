<!--transaction type modal-->
    <div class="modal fade" id="transactionTypeModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="inputModalTitle">New Transaction Type</h4>
          </div>
          <div class="modal-body">
            <form class="form" id="transactionTypeForm" role="form" action="{{route('newTransactionType')}}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="transactionType">Category</label>
                <input type="text" class="form-control" id="transactionType" name="transactionType" placeholder="" required>
              </div>
              <div class="form-group">
                <label for="transactionDesc">Description</label>
                <input type="text" class="form-control" id="transactionDesc" name="transactionDesc" placeholder="Descriptions" required>
              </div>
              <div class="form-group">
                <label for="transactionSubtype">Detailed Category</label>
                <select class="form-control" id="transactionDesc" name="transactionDesc">
                  <option value = 'Y'>Income</option>
                  <option value = 'N'>Expenses</option>
                </select>
              </div>
              <div class="hidden" id="errors" >
                <ul></ul>
              </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">New</button>
          </div>
        </form>
      </div>
    </div>
  </div><!--transaction type modal-->
