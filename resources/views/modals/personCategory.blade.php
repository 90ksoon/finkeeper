<!--Category type modal Start-->
<div class="modal fade" id="personCategoryModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="inputModalTitle">Personalize Categories</h4>
      </div>


      <div class="modal-body">
        <div class="container-fluid">

          {{-- for setting up new transaction boilerplate --}}

          @isset($newSetup)

          <h4>Select Categories to Keep Track Your Personal Finance</h4>
          <form class="form" id="personCategoryForm" role="form" action="{{route('newTransactionType')}}" method="post">
            {{ csrf_field() }}
            <div class="col-xs-12-l-6">
              <div class="row">
                @foreach($newSetup as $indx => $item)
                <div class="col-xs-6 text-center">
                  <div data-toggle="buttons">
                    <div class="form-group">
                      <label class="btn btn-margin-block @if($item->t_income_flag == 'Y') alert-info @else alert-warning @endif " for="{{$item->t_type_name}}_id">
                        <input type="checkbox" class="hidden" id="{{ $item->t_type_name}}_id" name="transactionType[]" value="{{ $item->t_type_name }}" > {{$item->t_type_name}}
                      </label>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
            @include('layouts.errors')
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">New</button>
            </div>
          </form>
          @else

          {{-- for editing section upon newly create --}}

          <div class="container-fluid">

              <div class="row">
                @foreach($transaction_type as $indx => $item)
                <div class="col-xs-6 text-center">
                      <label class="btn btn-margin-block @if($item->income == 'Y') alert-blue @else alert-orange @endif " for="{{$item->transtype_name}}_id">{{$item->transtype_name}}
                </label>
                  </div>
                @endforeach
              </div>
          </div>

          <form class="form" id="transactionTypeForm" role="form" action="{{route('newTransactionType')}}" method="post">
            {{ csrf_field() }}
          <div class="form-group">
            <label for="transactionType">Category</label>
            <input type="text" class="form-control" id="transactionType" name="transactionType" placeholder="" required>
          </div>
          <div class="form-group">
            <label for="transactionDesc">Description</label>
            <input type="text" class="form-control" id="transactionDesc" name="transactionDesc" placeholder="Descriptions" required>
          </div>
          <div class="form-group">
            <label for="transactionSubtype">Detailed Category</label>
            <select class="form-control" id="transactionDesc" name="transactionDesc">
              <option value = 'Y'>Income</option>
              <option value = 'N'>Expenses</option>
            </select>
          </div>
          <div class="hidden" id="errors">
            <ul></ul>
          </div>

          @include('layouts.errors')

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            @empty($newSetup)
            <a href="{{ route('editTransaction') }}">
              <button type="button" class="btn btn-default">
                Edit Category
              </button>
              </a>
            @endempty
            <button type="submit" class="btn btn-primary">New</button>
          </div>
          </form>
          @endisset

        </div>
      </div>
    </div>
  </div>
</div>
<!--Category type modal End-->
