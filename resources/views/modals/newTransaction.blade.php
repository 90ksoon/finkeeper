<!--new transcation modal start-->
    <div class="modal fade" id="transactionModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="inputModalTitle">Add Transaction</h4>
          </div>
          <div class="modal-body">
            <form class="form" id="transactionForm" role="form" action="{{ route('newTransaction')}}" method="post">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="amount">Transaction Amount</label>
                <input type="number" min="0.00" step="any" class="form-control" id="transactionAmount" name="amount" placeholder="RM" required>
              </div>
                  @if(count($transaction_type) > 0 )
                  <div class="form-group">
                    <label for="trantype">Category</label>
                  <select class="form-control" id="trantype" name="transaction_type" placeholder="" required>
                    @foreach($transaction_type as $ttype)
                      <option value="{{ $ttype->id }}"
                        class="
                        @if($ttype->income == 'Y') alert-blue
                        @else alert-orange
                        @endif
                        ">
                        {{ $ttype->transtype_name }}
                      </option>
                    @endforeach
                  </select>
                </div>
                    <div class="form-group">
                      <label for="transubtype">Detailed Category</label>
                        <select class="form-control" id="transubtype" name="transaction_subtypes">
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="remarks">Remarks</label>
                      <div class="input-medium" id="remarks_tag" data-tags-input-name="remarks"></div>
                      <p class="help-block">Tag it if its not ordinary expenses!</p>
                      <input type="hidden" name="remarks[]" value="">
                    </div>
                  @endif
              <div class="hidden" id="errors" >
                <ul></ul>
              </div>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info" data-dismiss="modal" data-target="#transactionTypeModal" data-toggle="modal">New Category</button>
            <button type="submit" class="btn btn-primary">New</button>
          </div>
        </form>
      </div>
    </div>
  </div><!--new transaction modal end-->
