const gulp = require('gulp');
const elixir = require('laravel-elixir');

gulp.task('default', function() {
  // place code for your default task here
});

elixir(function(mix){

  mix.browserSync({
    proxy: '127.0.0.1:8000'
  });

});

elixir(function(mix){
  mix.sass("app.scss");
})
