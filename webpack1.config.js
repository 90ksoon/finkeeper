const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const webpack = require('webpack');
// const jquery = require('jquery');

module.exports = {
  entry: ['./resources/assets/js/app.js',
  './node_modules/jquery/dist/jquery.js',
  './node_modules/chart.js/dist/Chart.js'],
  output: {
    filename: 'bundle.js',
    path: __dirname +'/public/js'
  },
  plugins: [
    new BrowserSyncPlugin ({
      host : 'localhost',
      port : 3000,
      proxy : '127.0.0.1:8000',
      // server: { baseDir: ['public']}

    },
    {
      reload: true
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ],
  watch:true
  // resolve: {
  //   alias : {
  //     jquery: 'jquery/src/jquery'
  //   }
  // }
}
