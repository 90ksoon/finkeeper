<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@index');

Route::get('/{savings}', 'HomeController@getCharting');

Route::get('/{expenses}', 'HomeController@getCharting');

Route::get('/{impulsive}', 'HomeController@getCharting');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard','DashBoardController@show')->name('dashboard');

Route::get('/user/{id}','UserController@show');

Route::get('/user','UserController@public');

Route::post('/transaction/newType','TransactionTypeController@create')->name('newTransactionType');

Route::post('/transaction/newSubtype','TransactionSubtypeController@create')->name('newTransactionSubtype');

Route::post('/transaction/checkSubtype/{id}','TransactionSubtypeController@index')->name('checkTransactionSubtype');

Route::get('/admin','AdminController@show')->middleware('can:admin')->name('admin');

Route::post('/transaction/new','TransactionController@create')->name('newTransaction');

Route::get('/transaction/edit', 'TransactionTypeController@edit')->name('editTransaction');

Route::get('/test','HomeController@test');
